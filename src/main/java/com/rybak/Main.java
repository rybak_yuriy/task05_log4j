package com.rybak;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {

    private static Logger log = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        log.info("Info message");
        log.trace("Trace message");
        log.debug("Debug message");
        log.warn("Warn message");
        log.error("Error message");
        log.fatal("Fatal message");
    }
}
