package com.rybak;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {

    public static final String ACCOUNT_SID = "ACd10013f7b40c00f7dde134a93dd617d3";
    public static final String AUTH_TOKEN = "31f39d0b6715fdc57b52f615fab6a794";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380970038257"),
                        new PhoneNumber("+12024108356"), str).create();
    }
}
